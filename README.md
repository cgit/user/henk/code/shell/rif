Hello!

Are you a real IPv6 friend?

## Introduction

People like you and me don't want or need Facebook. However, we do
want to securely communicate with our friends. And chat with them. And
know when they are online. Let's solve this problem once-and-for-all
in a decentralised, sustainable and future proof way.

## Here comes the real IPv6 friend

Instead of using a single system or server, we use IPv6 to connect to
our friends directly. Because each of our friends is reachable by IPv6
if they are online (otherwise they would not be a real IPv6 friend),
we can easily verify if they are online. To find out who is online,
just connect to their computer!

## How it works

It's a bit geeky, but it works actually rather simple.
To become a real IPv6 friend, you need to do the following things:

- Generate a GPG key with a comment named **RI6F** following the URL of
  your computer (f.i. **RI6F https://nico.ungleich.cloud**)
- Ensure that the URL points to the IPv6 address of your computer
- Setup a webserver on your computer
- Export your key and all real IPv6 friend keys to your webserver as
  "rifkeys.txt
- For each of your friends, check whether they are online
- Import the friends of your friends and check whether they are
  online, too!
- Send friend requests (like other platforms!)

### Example: Generating a key with the right comment

In this example I choose to create an ECC based key that requires the
export option in gpg:

```
[1:24] line:~% gpg --expert --full-gen-key
gpg (GnuPG) 2.2.12; Copyright (C) 2018 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

Please select what kind of key you want:
   (1) RSA and RSA (default)
   (2) DSA and Elgamal
   (3) DSA (sign only)
   (4) RSA (sign only)
   (7) DSA (set your own capabilities)
   (8) RSA (set your own capabilities)
   (9) ECC and ECC
  (10) ECC (sign only)
  (11) ECC (set your own capabilities)
  (13) Existing key
Your selection? 9
Please select which elliptic curve you want:
   (1) Curve 25519
   (3) NIST P-256
   (4) NIST P-384
   (5) NIST P-521
   (6) Brainpool P-256
   (7) Brainpool P-384
   (8) Brainpool P-512
   (9) secp256k1
Your selection? 1
Please specify how long the key should be valid.
         0 = key does not expire
      <n>  = key expires in n days
      <n>w = key expires in n weeks
      <n>m = key expires in n months
      <n>y = key expires in n years
Key is valid for? (0) 0
Key does not expire at all
Is this correct? (y/N) y

GnuPG needs to construct a user ID to identify your key.

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? c
Comment: RI6F https://nico.ungleich.cloud
You selected this USER-ID:
    "Nico Schottelius (RI6F https://nico.ungleich.cloud) <ipv6@nico.ungleich.cloud>"

Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? o
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
We need to generate a lot of random bytes. It is a good idea to perform
some other action (type on the keyboard, move the mouse, utilize the
disks) during the prime generation; this gives the random number
generator a better chance to gain enough entropy.
gpg: key 27541E11E73F288D marked as ultimately trusted
gpg: directory '/home/nico/.gnupg/openpgp-revocs.d' created
gpg: revocation certificate stored as '/home/nico/.gnupg/openpgp-revocs.d/B982A8BABC030C66DEF5984527541E11E73F288D.rev'
public and secret key created and signed.

pub   ed25519 2019-09-09 [SC]
      B982A8BABC030C66DEF5984527541E11E73F288D
uid                      Nico Schottelius (RI6F https://nico.ungleich.cloud) <ipv6@nico.ungleich.cloud>
sub   cv25519 2019-09-09 [E]


```

### Example: Setting up a webserver

On a Debian/Devuan based distro:

```
apt install nginx
sudo chown $(whoami) /var/www/html
```


### Exporting all keys

To make yourself accessible and expose who your real IPv6 friends are.

```
gpg -a --export RI6F > /var/www/html/rifkeys
```

### Example: List your friends

You can use the included rif-listfriends.sh or directly gpg:

```
[1:40] line:~% gpg --list-keys --with-colons | grep RI6F | awk -F: '{ print $10 }' | sed 's/\\x3a/:/'
Nico Schottelius (RI6F https://nico.ungleich.cloud) <ipv6@nico.ungleich.cloud>
Nico Schottelius (myself) (RI6F https://nico2.ungleich.cloud) <nico@nico.ungleich.cloud>
```


### Example: Checking which friends are online

Use the included rif-checkfriends.sh script or iterate yourself over
above output.

```
[1:54] line:realipv6friend% sh rif-checkfriends.sh
Checking Nico Schottelius  on https://nico.ungleich.cloud ...
Nico Schottelius is online
Checking Nico Schottelius (myself)  on https://nico2.ungleich.cloud ...
Nico Schottelius (myself) is offline
[1:54] line:realipv6friend%

```

### Example: Importing friends of my friend

Importing friends of a friend is as simple as importing all the
exported keys! We import friends directly from the URL of a friend:

```
curl -6 -s https://nico.ungleich.cloud/rifkeys | gpg --import
```

You can also use the included rif-importfriends.sh.


### Example: Updating your friends

Now that you have imported the friends of your friends, they have
automatically also become your friends!

Obviously, you will want to check whether you even want to sign some
of the friends (keys) that you downloaded, but clearly you want to
show off with the biggest amount of friends (that's what is most
important anyway, isn't it?).

In any case, to update your friend list, simply repeat the command you
ran above already in the beginning:

```
gpg -a --export RI6F > /var/www/html/rifkeys
```

Or use the included `rif-exportfriends.sh`

### Example: Importing friends of all friends and exporting them

From time to time you might want to checkup on which friends gained
new friends and add them to your friend list. This is as easy as
iterating over all your friends and importing their friends into your
friends list. And because you are a good friend, you might want to
export all friends again.

To make things easy, the included a script `rif-friendimportexport.sh`
automates this process.


### Example: Sending a message to a friend

You can probably just use your mail program for that - and you might
even be able to send it IPv6 only!

### Example: Invite a friend

Live is not great without friends! Let's invite a friend! This is as
easy is this:

```
printf 'Hello yournamehere!\n I am looking for real IPv6 friends.\n You find my friends on https://nico.ungleich.cloud/rifkeys.\nHow real IPv6 friends find each other is explained on https://code.ungleich.ch/nico/rif.\n Looking forward to befriend you!' | mail -s "friend request" ipv6@your.friend.example.com
```

And obviously this is waaaaaaaaay to long to type. That's why
`rif-invitefriends.sh` puts this nicely into a script.

Attention: this requires a working mail setup on your computer
(i.e. outgoing emails sending by `mail` works).

## Finding friends

As real IPv6 friends are decentralised, there is no central registry
of friends. Just startup the webserver on your machine, export your
key(s) and ping your friend.

With every friend that is added to your network, the reach grows in
the chain of all friends of every friend!

So just start in your small cycle and forward your friend URL to your
friends!


## About

Real IPv6 friend is a project launched by Nico Schottelius on 2019-09-10.
If you want to become my friend, my friend URL is https://nico.ungleich.cloud.
