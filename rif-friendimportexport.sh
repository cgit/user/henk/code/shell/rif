#!/bin/sh
# Nico Schottelius, 2019-09-10, Seoul

tmp=$(mktemp)

gpg --list-keys --with-colons | grep RI6F | awk -F: '{ print $10 }' | sed 's/\\x3a/:/' | (
    while read line
    do
        name=$(echo $line | sed 's/\(.*\)(.*/\1/')
        url=$(echo $line | sed -e 's/.*(RI6F //' -e 's/).*//')
        keyurl=$url/rifkeys

        echo "Getting friends from $name on $keyurl ..."
        curl -6 -s "$keyurl" >> $tmp
    done
)


gpg --import "$tmp"
rm -f "$tmp"
