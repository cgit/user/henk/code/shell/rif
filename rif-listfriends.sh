#!/bin/sh
# 2019-09-10, Nico Schottelius, Seoul

gpg --list-keys --with-colons | awk -F: '/RI6F/ { print gensub("\\\\x3a", ":", "g", $10) }'
