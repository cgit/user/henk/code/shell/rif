#!/bin/sh
# 2019-09-10, Nico Schottelius, Seoul

if [ $# -ne 4 ]; then
    echo "$0: <friendname> <friendemail> <your URL> <your name>"
    echo 'F.i. $0 nico ipv6@nico.ungleich.cloud https://your-url.example.com'
    exit 1
fi

friend=$1; shift
email=$1;  shift
url=$1;    shift
name=$1;   shift

keyurl=$url/rifkeys
projecturl=https://code.ungleich.ch/nico/rif

cat <<EOF | mail -s "Real IPv6 friend request" "${email}"
Hello ${friend},

I want to invite you to become my real IPv6 friend.
You can find my friends including my key on ${keyurl}.

On $projecturl you can find out how to become my friend.

Looking forward to hearing from you,

$name
EOF
