#!/bin/sh
# 2019-09-10, Nico Schottelius, Seoul

if [ $# -lt 1 ]; then
    echo "$0: <URL> [URL...]"
    echo "URL: of your real IPv6 friend(s)"
    exit 1
fi

while [ $# -ge 1 ]; do
    url=$1; shift
    fullurl=${url}/rifkeys

    curl -6 -s "${fullurl}" | gpg --import
done
