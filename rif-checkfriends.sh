#!/bin/sh
# 2019-09-10, Nico Schottelius, Seoul

gpg --list-keys --with-colons | grep RI6F | awk -F: '{ print $10 }' | sed 's/\\x3a/:/' | (
    while read line
    do
        name=$(echo $line | sed 's/\(.*\)(.*/\1/')
        # Assume by default offline
        online=offline
        url=$(echo $line | sed -e 's/.*(RI6F //' -e 's/).*//')
        echo "Checking $name on $url ..."
        curl -6 -s "$url" > /dev/null && online=online
        echo $name is $online
    done
)
